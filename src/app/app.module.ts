import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SignInPage } from '../pages/sign-in/sign-in';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { ForgetPasswordPage } from '../pages/forget-password/forget-password';
import { AfterLoginPage } from '../pages/after-login/after-login';
import { MessagesPage } from '../pages/messages/messages';
import { HeaderPage } from '../pages/header/header';
import { FooterPage } from '../pages/footer/footer';
import { FollowRequestPage } from '../pages/follow-request/follow-request';
import { ProfilePage } from '../pages/profile/profile';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SignInPage,
    SignUpPage,
    ForgetPasswordPage,
    AfterLoginPage,
    MessagesPage,
    HeaderPage,
    FooterPage,
    FollowRequestPage,
    ProfilePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SignInPage,
    SignUpPage,
    ForgetPasswordPage,
    AfterLoginPage,
    MessagesPage,
    HeaderPage,
    FooterPage,
    FollowRequestPage,
    ProfilePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider
  ]
})
export class AppModule {}
