export interface SignUpUser{
    fname:string;
    lname:string;
    email:string;
    phone:number;
    password:string;
    confirmPassword:string;
}