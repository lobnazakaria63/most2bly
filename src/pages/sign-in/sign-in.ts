import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../model/user';
import { ForgetPasswordPage } from '../forget-password/forget-password';
import { AfterLoginPage } from '../after-login/after-login';

/**
 * Generated class for the SignInPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-in',
  templateUrl: 'sign-in.html',
})
export class SignInPage {
  user={} as User;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignInPage');
  }
  toForgot(){
    this.navCtrl.push(ForgetPasswordPage);
  }
  toAfterLogin(){
    this.navCtrl.setRoot(AfterLoginPage);
  }

}
