import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Posts } from '../../model/posts';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  posts: Posts[]=[
    {
      date_blue:"20/1/2018",
      date_white:"20/10/2017",
      name:"اسم اول",
      img:"hipster",
      post:" أندرسون (71 عامًا) ركضا محاولين الهرب خارج منزلهما بمدينة كينغستون، بعد أن أُشعلت بهما النيران، إلى أن سقطا على بعد إقدام من المنزل، يوم الجمعة الماضي."

    },
    {
      date_blue:"11/11/2017",
      date_white:"12/5/2016",
      name:"اسم تاني",
      img:"hipster",
      post:"أندرسون (71 عامًا) ركضا محاولين الهرب خارج منزلهما بمدينة كينغستون، بعد أن أُشعلت بهما النيران، إلى أن سقطا على بعد إقدام من المنزل، يوم الجمعة الماضي."
    },
    {
      date_blue:"5/3/2018",
      date_white:"2/2/2018",
      name:"اسم ثالث",
      img:"hipster",
      post:"أندرسون (71 عامًا) ركضا محاولين الهرب خارج منزلهما بمدينة كينغستون، بعد أن أُشعلت بهما النيران، إلى أن سقطا على بعد إقدام من المنزل، يوم الجمعة الماضي."
    },
    {
      date_blue:"12/3/2017",
      date_white:"12/3/2018",
      name:"اسم رابع",
      img:"hipster",
      post:"أندرسون (71 عامًا) ركضا محاولين الهرب خارج منزلهما بمدينة كينغستون، بعد أن أُشعلت بهما النيران، إلى أن سقطا على بعد إقدام من المنزل، يوم الجمعة الماضي."
    }
  ]
  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }
  toPost(){
    
  }
  profileChoose() {
    const confirm = this.alertCtrl.create({
      title: 'Choose image source..',
      buttons: [
        {
          text: 'Camera',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Gallary',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }
}
