import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MsgsFrom } from '../../model/msgs';

/**
 * Generated class for the MessagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html',
})
export class MessagesPage {
  msgs:string="All"
  unknowns:MsgsFrom[]=[
    {
      name:"مجهول",
      img:"hipster"
    },
    {
      name:"مجهول",
      img:"hipster"
    },
    {
      name:"مجهول",
      img:"hipster"
    },
    {
      name:"مجهول",
      img:"hipster"
    },
    {
      name:"مجهول",
      img:"hipster"
    },
    {
      name:"مجهول",
      img:"hipster"
    }
  ]
  friends:MsgsFrom[]=[
    {
      name:"صديق اول",
      img:"hipster"
    },
    {
      name:"صديق تاني",
      img:"hipster"
    },
    {
      name:"صديق ثالث",
      img:"hipster"
    },
    {
      name:"صديق رابع",
      img:"hipster"
    },
    {
      name:"صديق خامس",
      img:"hipster"
    },
  ]
  All:MsgsFrom[]=[
    {
      name:"صديق اول",
      img:"hipster"
    },
    {
      name:"صديق تاني",
      img:"hipster"
    },
    {
      name:"صديق ثالث",
      img:"hipster"
    },
    {
      name:"صديق رابع",
      img:"hipster"
    },
    {
      name:"صديق خامس",
      img:"hipster"
    },
    {
      name:"مجهول",
      img:"hipster"
    },
    {
      name:"مجهول",
      img:"hipster"
    },
    {
      name:"مجهول",
      img:"hipster"
    },
    {
      name:"مجهول",
      img:"hipster"
    },
    {
      name:"مجهول",
      img:"hipster"
    },
    {
      name:"مجهول",
      img:"hipster"
    },
  ]
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessagesPage');
  }

}
