import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FollowRequestPage } from './follow-request';

@NgModule({
  declarations: [
    FollowRequestPage,
  ],
  imports: [
    IonicPageModule.forChild(FollowRequestPage),
  ],
})
export class FollowRequestPageModule {}
