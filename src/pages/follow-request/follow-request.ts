import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FollowRequest } from '../../model/followRequest';
import {AlertController} from 'ionic-angular';
import * as $ from "jquery";
/**
 * Generated class for the FollowRequestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-follow-request',
  templateUrl: 'follow-request.html',
})
export class FollowRequestPage implements OnInit {
  follow:FollowRequest[]=[
    {
      name:"صديق اول",
      img:"hipster"
    },
    {
      name:"صديق تاني",
      img:"hipster"
    },
    {
      name:"صديق ثالث",
      img:"hipster"
    },
    {
      name:"صديق رابع",
      img:"hipster"
    },
    {
      name:"صديق خامس",
      img:"hipster"
    },
  ]
  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController) {
  }
  ngOnInit(){
   
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FollowRequestPage');
    /*$("#removebtn").click(function(){
      $(this).parent().find(".acc-btn").css("display", "none");
      $(this).parent().find("#deletebtn").css("display", "block");
  
  });

$("#addbtn").click(function(){
      $(this).parent().find(".acc-btn").css("display", "none");
      $(this).parent().find("#acceptbtn").css("display", "block");    
      
  });*/
  }
  removeReq(index){
    
    $('#removebtn'+index).css({"display":"none"});
        $('#addbtn'+index).css({"display":"none"});
        $('#deletebtn'+index).css({"display":"block"});
   }
 
  addReq(index){
    
   
    $('#removebtn'+index).css({"display":"none"});
    $('#addbtn'+index).css({"display":"none"});
    $('#acceptbtn'+index).css({"display":"block"});
  }
}

